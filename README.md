# GitLab Serverless examples

This projects shows you some example serverless functions supported by GitLab.

* `echo` based on the riff project / classical runtime that echos the data received, and some environment variables if present
* `error` based on the riff project / classical runtime that always throws an error
* `echo` based on the openfaas runtime

## Calling these functions

The riff runtimes should by called by `POST`, the openfaas runtime can be called by `GET` too. Example:

    curl -v --header "Content-Type: application/json" --data '{"GitLab":"FaaS"}' http://functions-echo-js.namespace.example.com/

You should be aware of how `console.log` is handled by the runtimes. OpenFaas returns everything that gets logged, while
the riff runtimes send logs to the Knative logs, and these can be processed by a log collector.

## Using the example with Kubernetes secrets

The `echo` function returns all the environment variables starting with `MY_`, including Kubernetes secrets passed to
the function as environment variables.

The given secret should be created under the custom namespace used to deploy your functions. To find out the namespace, run `kubectl get namespaces`. The custom namespace is of the form `<project name>-<project id>-<environment>`. Once you have the namespace, you can create the secret using `kubectl create secret generic my-secrets -n <my namespace> --from-literal=MY_SECRET=imverysecure` under your custom namespace.

## OpenFaas quick start

1. Install `faas-cli`: `brew install faas-cli`
1. List available templates: `faas-cli template store list`
1. Install the node template (and all the classic templates): `faas-cli template store pull node10`
1. The resulting `template` dir is gitignored. Move the `node` folder to become your `source` from your `serverless.yml`

## Getting started with GitLab Serverless

For more information, read [our documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/).
